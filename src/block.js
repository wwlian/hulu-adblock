// Copyright 2013 Wilson Lian

var listener = function(details) {
  console.log("Redirected ad request for " + details.url);
  return {"redirectUrl": "http://huluads.herokuapp.com/foo.mp4"}
};
var filter = {"urls": ["*://ads.hulu.com/*.mp4", "*://ll.a.hulu.com/*.mp4", 
  "*://a.huluad.com/*.mp4"]};
chrome.webRequest.onBeforeRequest.addListener(listener, filter, ["blocking"]);

